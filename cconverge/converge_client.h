#ifndef _CONVERGE_CLIENT
#define _CONVERGE_CLIENT

#define TEXT_SIZE 250
#define MAX_NOTARY 10
#define CERT_SIZE 2000
#define RESP_SIZE 10000

typedef struct converge_config {
	char db[TEXT_SIZE];
	char userdb[TEXT_SIZE];
	char notaries_dir[TEXT_SIZE];
	char notary_agreement[50];
} converge_config;

typedef struct notary {
	char host[TEXT_SIZE];
	int http_port;
	int ssl_port;
	char cert[CERT_SIZE];
} notary;

typedef struct notaries {
	int version;
	notary n[MAX_NOTARY];
	int num_hosts;
	char name[TEXT_SIZE];
	char bundle[TEXT_SIZE];
	struct notaries *n_next;
} notaries;

typedef struct notary_msg {
	int num_note;
	notaries *note;
	unsigned int resp_index;
	char resp[RESP_SIZE];
} notary_msg;

int converge_client_do_verify(const char *host, const char *port, const char *print);
size_t _notary_cb(void *buffer, size_t size, size_t nmemb, void *userp);
int _resp_valid(const char *resp, const char *cert);
void _update_cache(const char *host, const char *port, const char *print);
int _check_cache(const char *host, const char *port, const char *print);

void *xmalloc(unsigned int);
void converge_client_init();
void converge_client_clean();
void _load_config();
notaries *_load_notary(const char *s, notaries *n);
char *_safe_load(const char *s);
void _make_like_python(char *s);

#endif
