CREATE TABLE IF NOT EXISTS connect (pid int, grp int, ip text, port int, fd int, 
	CONSTRAINT connect_unique UNIQUE(pid, grp, fd) ON CONFLICT REPLACE);

CREATE TABLE IF NOT EXISTS getaddrinfo (pid int, grp int, host text, ip text);

.exit
